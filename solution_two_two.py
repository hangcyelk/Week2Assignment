from TurtleWorld import *
from math import *
world = TurtleWorld()
bob = Turtle()

def polyline(t, n, length, angle):
	for i in range(n):
		fd(t, length)
		lt(t, angle)

def arc(t, r, angle):
	arc_length = 2 * pi * r * angle / 360
	n = int(arc_length / 3) + 1
	step_length = arc_length / n
	step_angle = float(angle) / n
	lt(t, step_angle/2)
	polyline(t, n, step_length, step_angle)
	rt(t, step_angle/2)

def Leaves(t, r,angle):
	for i in range(2):
		arc(t, r, angle)
		lt(t, 180-angle)

def TurtleFlower(t,n,r,angle):
	for i in range (n):
		Leaves(t,r,angle)
		lt(t, 360.0/n)

def ChangeLocation(t, distance):
	pu(t)
	fd(t,distance)
	pd(t)

ChangeLocation(bob, -50)
TurtleFlower(bob, 5, 60.0,60.0)

ChangeLocation(bob, 50)
TurtleFlower(bob, 10, 40.0,80.0)

ChangeLocation(bob, 50)
TurtleFlower(bob, 15, 140.0,20.0)

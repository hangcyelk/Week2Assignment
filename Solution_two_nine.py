
# coding: utf-8

# In[4]:


def ack(a,b):
    if a == 0:
        return b+1
    elif a>0 and b == 0:
        return ack(a-1,1)
    elif a>0 and b>0:
        return ack(a-1, ack(a,b-1))

ack(3,4)
# if a,b are larger number, we'll have a runtime error cause it's recursing forever.


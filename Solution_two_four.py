from TurtleWorld import *
from math import *

world = TurtleWorld()
bob = Turtle()
bob.delay = 0.01

def polyline(t, n, length, angle):
	for i in range(n):
		fd(t, length)
		lt(t, angle)

def polygon(t,n,length):
	angle = 360.0/n
	polyline(t,n,length,angle)

def arc(t, r, angle):
	arc_length = 2 * pi * r * angle / 360
	n = int(arc_length / 3) + 1
	step_length = arc_length / n
	step_angle = float(angle) / n
	lt(t, step_angle/2)
	polyline(t, n, step_length, step_angle)
	rt(t, step_angle/2)

def circle(t, r):
	arc(t, r, 360)

def bias(t, x, y):
	""" draw a bias with given x and y offsets"""
	angle = atan2(y,x)*180/pi
	length = sqrt(x**2+y**2)
	lt(t, angle)
	fd(t,length)
	bk(t,length)
	rt(t,angle)


def bar(t, n, height):
	"""draw a bar at a given height and return"""
	climb(t,n*height)
	fd(t,n)
	bk(t,n)
	climb(t,-n*height)

def climb(t,n):
	lt(t)
	pu(t)
	fd(t,n)
	pd(t)
	rt(t)	

def semicircle(t,n,height):
	climb(t, n*height)
	arc(t,n/2.0,180)
	lt(t)
	fd(t,n*height+n)
	lt(t)

def fdbk(t,n):
	fd(t,n)
	bk(t,n)

def hangman(t, n, height):
    climb(t, n * height)
    fdbk(t, n)
    lt(t)
    bk(t, n*height)
    rt(t)

def draw_a (t,n):
	bias(t, n/2, 2*n)
	bar(t, n , 1)
	pu(t)
	fd(t,n)
	pd(t)
	bias(t, -n/2, 2*n)

def draw_b(t,n):
	semicircle(t,n,1)
	semicircle(t,n,0)

def draw_c(t,n):
	lt(t,180)
	arc(t,n,220)

def draw_d(t,n):
	semicircle(t,2*n,0)

def draw_e(t,n):
	hangman(t,n,2)
	hangman(t,n,1)
	fd(t,n)

def draw_f(t,n):
	hangman(t,n,2)
	hangman(t,n,1)	

def draw_g(t,n):
	hangman(t,n,2)
	fd(t,n/2)
	bar(t,n/2,2)
	fd(t,n/2)
	lt(t)
	fdbk(t,n)
	rt(t)

def draw_h(t,n):
	lt(t)
	fdbk(t,2*n)
	rt(t)
	hangman(t,n,1)
	pu(t)
	fd(t,n)
	pd(t)
	lt(t)
	fdbk(t,2*n)
	rt(t)

def draw_i(t,n):
	bar(t,n,2)
	fd(t,n/2)
	lt(t)
	fdbk(t,2*n)
	rt(t)
	fd(t,n/2)
	

def draw_j(t,n):
	bar(t,n,2)
	arc(t,n/2,90)
	fd(t,n/2*3)
	pu(t)
	fd(t,-2*n)
	pd(t)
	rt(t)
	pu(t)
	fd(t,n/2)
	pd(t)

draw_j(bob,20)


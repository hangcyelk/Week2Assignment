from TurtleWorld import *
from math import *
world = TurtleWorld()
bob = Turtle()

def polyline(t, n, length, angle):
	for i in range(n):
		fd(t, length)
		lt(t, angle)

def polygon(t,n,length):
	angle = 360.0/n
	polyline(t,n,length,angle)

def trianglePiece(t,r,angle):
	y = r*sin(angle*pi/180)
	rt(t,angle)
	fd(t,r)
	lt(t,90+angle)
	fd(t,2*y)
	lt(t, 90+angle)
	fd(t,r)
	lt(t, 180-angle)

def pie(t,n,r):
	angle = 360.0/n
	for i in range(n):
		trianglePiece(t,r,angle/2)
		lt(t, angle)

def TurtlePie(t,n,r):
	pie(t,n,r)
	pu(t)
	fd(t, r*2 + 10)
	pd(t)

TurtlePie(bob, 5, 50)
wait_for_user()
TurtlePie(bob, 6, 50)
wait_for_user()
TurtlePie(bob, 7, 50)


from TurtleWorld import *
from math import *
world = TurtleWorld()
bob = Turtle()
bob.delay = 0.01

def koch(t,n):
	if n<3:
		fd(t,n)
		return "end"
	small = n/3.0
	koch(t,small)
	lt(t,60)
	koch(t,small)
	rt(t,120)
	koch(t,small)
	lt(t,60)
	koch(t,small)

def snowflake(t,n):
	for i in range(3):
		koch(t,n)
		rt(t,120)

snowflake(bob,150)

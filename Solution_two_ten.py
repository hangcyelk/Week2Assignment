
# coding: utf-8

# In[26]:


def is_palindrome(a):
    length = len(a)
    if length == 0:
        return True
    elif a[0] != a[-1]:
        return False
    else:
        return is_palindrome(a[1:-1])

is_palindrome('noon')


# In[17]:


'noon'[1:-1]


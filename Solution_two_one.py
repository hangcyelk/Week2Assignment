from TurtleWorld import *
from math import *
world = TurtleWorld()
bob = Turtle()

def polyline(t, n, length, angle):
	"""Draws n line segments with the given length and
    angle (in degrees) between them.  t is a turtle.
    """
    for i in range(n):
        fd(t, length)
        lt(t, angle)

def polygon(t, n, length):
	""" Draws a n-side polygon with given length, t is a turtle"""
    angle = 360.0 / n
    polyline(t,n,length,angle)

def arc(t, r, angle):
	"""Draws an arc with given angle and radius, the arc is 
	actually serveral line segments"""
    arc_length = 2 * pi * r * angle / 360
    n = int(arc_length / 3) + 1
    step_length = arc_length / n
    step_angle = float(angle) / n
    polyline(t, n, step_length, step_angle)

def circle(t, r):
	""" draw a circle"""
    arc(t, r, 360)

circle(bob, 20)